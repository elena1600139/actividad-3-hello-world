using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsuleScaling : MonoBehaviour
{
    [SerializeField] 
    private Vector3 axes; 
    public float scaleUnits;  

    // Update is called once per frame
    void Update()
    {
        //Acotaci�n de los valores de escalado al valor unitario [ -1, 1]
        axes = capsulemovement.ClampVector3(axes);

        // La  escala, al contrario que la rotaci�n y el movimiento, es acumulativa 
        // lo que quiere decir que debemos a�adir el nuevo valor de la escala, al valor anterior.
        transform.localScale += axes * (scaleUnits * Time.deltaTime);

    }
}
